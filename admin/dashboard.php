<!--
=========================================================
 Paper Dashboard 2 - v2.0.0
=========================================================

 Product Page: https://www.creative-tim.com/product/paper-dashboard-2
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard/blob/master/LICENSE)

 Coded by Creative Tim

=========================================================

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php
session_start();
    if ($_SESSION['login'] != TRUE){
        header('Location: admin.php');
        exit;
    }
    ?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <link  rel="icon"  href="../image/favicon.ico"  type="image/x-icon">
  <title>
    Dashboard Gaiago
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <script src="https://kit.fontawesome.com/dd106b3ef2.js" crossorigin="anonymous"></script>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="./assets/css/paper-dashboard.css?v=2.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="./assets/css/demo.css" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper">
    <div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="../image/home_logo_admin.png">
          </div>
        </a>
        <a href="#" class="simple-text logo-normal">
          @ Charles Vaury
          <!-- <div class="logo-image-big">
            <img src="../assets/img/logo-big.png">
          </div> -->
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="active ">
            <a href="./dashboard.php">
              <i class="nc-icon nc-bank"></i>
              <p>Dashboard</p>
            </a>
          </li>

          <div class="dropdown-divider"></div>

          <li class="active">
              <a>
                <!-- <i class="nc-icon nc-diamond"></i> -->
                <i class="fas fa-seedling"></i>
                <p name="freen100" class="link">Free N100</p>
              </a>
          </li>

          <div class="dropdown-divider"></div>

          <li class="active">
              <a>
                <!-- <i class="nc-icon nc-diamond"></i> -->
                <i class="fas fa-seedling"></i>
                <p name="nutrigeo" class="link">Nutrigéo</p>
              </a>
          </li>

          <div class="dropdown-divider"></div>

          <li class="active">
              <a>
                <!-- <i class="nc-icon nc-diamond"></i> icon de base -->
                <!-- <i class="fas fa-exclamation-triangle"></i> icon en travaux -->
                <i class="fas fa-seedling"></i>
                <p name="freepk" class="link">Free PK</p>
              </a>
          </li>

          <div class="dropdown-divider"></div>

          <li class="active">
              <a>
                <!-- <i class="nc-icon nc-diamond"></i> icon de base -->
                <!-- <i class="fas fa-exclamation-triangle"></i> icon en travaux -->
                <i class="fas fa-seedling"></i>
                <p name="alasso" class="link">ALASSO</p>
              </a>
          </li>

          <div class="dropdown-divider"></div>

          <li class="active">
              <a>
                <!-- <i class="nc-icon nc-diamond"></i> icon de base -->
                <!-- <i class="fas fa-exclamation-triangle"></i> icon en travaux -->
                <i class="fas fa-seedling"></i>
                <p name="stimulus" class="link">STIMULUS</p>
              </a>
          </li>

          <div class="dropdown-divider"></div>

          <li class="active">
              <a>
                <!-- <i class="nc-icon nc-diamond"></i> icon de base -->
                <!-- <i class="fas fa-exclamation-triangle"></i> icon en travaux -->
                <i class="fas fa-seedling"></i>
                <p name="vitamin" class="link">VITAM'IN</p>
              </a>
          </li>
        
          <div class="dropdown-divider"></div>
          <li class="active">
            <a>
                <i class="fas fa-seedling"></i>
                <p name="datamap" class="link">Data Map</p>
            </a>
          </li>
          
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            
            
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
         
            <ul class="navbar-nav">
             
              
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-bank"></i> Menu
                  <p>
                    <span class="d-lg-none d-md-block">Navigation</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="https://gaiago.eu/" target="_blank" rel="noopener">Gaiago Home</a>
                  <a class="dropdown-item" href="../index.php">Page produit</a>
                </div>
              </li>
              
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <!-- <div class="panel-header panel-header-lg">

  <canvas id="bigDashboardChart"></canvas>


</div> -->
      <div class="content" id="body">
        <!-- contenu changeant -->

        <div class="jumbotron">

          <div class="container background-bienvenue">
            <h1 id="statut_page">Statut</h1>  
            <h4>Pages en ligne..  </h4>

            <div>
              <?php
                include "../inc/database.php";
                $dbh = $result->query("SELECT `Titre`,`Actif` FROM `page`");
                foreach($dbh as $row){
                  if($row['Titre'] == "Free N 100" && $row['Actif'] == "1"){
                    echo "<div class='card separator green'>Free N 100</div>";
                  } else if($row['Titre'] == "Free N 100" && $row['Actif'] == "0") {
                    echo "<div class='card separator red'>Free N 100</div>";
                  }
                  if($row['Titre'] == "Nutrigéo" && $row['Actif'] == "1"){
                    echo "<div class='card separator green'>Nutrigéo</div>";
                  } else if($row['Titre'] == "Nutrigéo" && $row['Actif'] == "0") {
                    echo "<div class='card separator red'>Nutrigéo</div>";
                  }
                  if($row['Titre'] == "Free PK" && $row['Actif'] == "1"){
                    echo "<div class='card separator green'>Free PK</div>";
                  } else if($row['Titre'] == "Free PK" && $row['Actif'] == "0") {
                    echo "<div class='card separator red'>Free PK</div>";
                  }
                  if($row['Titre'] == "Alasso" && $row['Actif'] == "1"){
                    echo "<div class='card separator green'>Alasso</div>";
                  } else if($row['Titre'] == "Alasso" && $row['Actif'] == "0") {
                    echo "<div class='card separator red'>Alasso</div>";
                  }
                  if($row['Titre'] == "Stimulus" && $row['Actif'] == "1"){
                    echo "<div class='card separator green'>Stimulus</div>";
                  } else if($row['Titre'] == "Stimulus" && $row['Actif'] == "0") {
                    echo "<div class='card separator red'>Stimulus</div>";
                  }
                  if($row['Titre'] == "Vitamin" && $row['Actif'] == "1"){
                    echo "<div class='card separator green'>Vitam'in</div>";
                  } else if($row['Titre'] == "Vitamin" && $row['Actif'] == "0") {
                    echo "<div class='card separator red'>Vitam'in</div>";
                  }

                }
              ?>
            </div>

          </div>

        </div>

        <div class="jumbotron jumb-blue">
        <div class="container">
          <h2>Bienvenue</h2>
          <p>Ici c'est votre espace administration.</p>
          <p>Trouvez ci-dessous une documentation sur l'utilisation de ce Dashboard ! </p>
          
          <!-- <p><a class="btn btn-primary btn-lg" href="../inc/navbar.php" role="button">test navbar page &raquo;</a></p> -->
        </div>
      </div>

      <div class="container">
        <!-- Example row of columns -->
        <div class="row">
          <div class="col-md-4 jumbotron">
            <div class="jumbotron jumb-white2 txt-card-docu">
            <h2>Le Statut</h2>
            <p>Cette partie vous indique quel sont les pages qui sont actuellement en ligne ou non.
            Elles sont activables dans leurs pages respectives suffi de les activer ou non en cliquant sur <strong>" désactivé la page "</strong> ou <strong>" activé la page "</strong>. </p> 
            </div>
            <img src="../image/statu.png" alt="description login">
            <!-- <p><a class="btn btn-outline-info" href="#statut_page" role="button">View details &raquo;</a></p> -->
            <hr class="hr-bottom-mix1">
          </div>
          <div class="col-md-4 jumbotron">
            <div class="jumbotron jumb-white2 txt-card">
            <h2>Mes images</h2>
            <p>Modifiez les images directement sur les pages concernées.<br> Les changements d'images supprimeront toutes celles qui seront actuellement mises en ligne, pour les remettre pensez à avoir des copies à votre disposition.</p>
            </div>
            <img src="../image/modif-image.png" alt="logo-image">
            <hr class="hr-bottom-4em">
            <!-- <p><a class="btn btn-outline-info" href="#" role="button">View details &raquo;</a></p> -->
          </div>
          <div class="col-md-4 jumbotron">
            <div class="jumbotron jumb-white2 txt-card-docu">
            <h2>Mes Vidéos</h2>
            <p>Modifiez les Vidéos directement sur les pages concernées. Vous avez la possibilité de changer les vidéo de 2 manières, la premiere est de mettre un lien https et la 2ème est de mettre un lien " Iframe " pour plus d'info voir l'image ci-dessous </p>
            </div>
            <hr class="hr-bottom-4em">
            <img src="../image/modif-video.png" alt="logo-image">
            
            <!-- <p><a class="btn btn-outline-info" href="#" role="button">View details &raquo;</a></p> -->
          </div>
        </div>

        <hr>

      </div>
        
        
      </div>
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <nav class="footer-nav">
              <ul>
                <li>
                  <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>
                </li>
                <li>
                  <a href="http://blog.creative-tim.com/" target="_blank">Blog</a>
                </li>
                <li>
                  <a href="https://www.creative-tim.com/license" target="_blank">Licenses</a>
                </li>
              </ul>
            </nav>
            <div class="credits ml-auto">
              <span class="copyright">
                ©
                <script>
                  document.write(new Date().getFullYear())
                </script>, made with <i class="fa fa-heart heart"></i> by Creative Tim, Sébastien Delalandre, Tourat Kévin
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="./assets/js/core/jquery.min.js"></script>
  <script src="./assets/js/core/popper.min.js"></script>
  <script src="./assets/js/core/bootstrap.min.js"></script>
  <script src="./assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="./assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/paper-dashboard.js" type="text/javascript"></script>
  
</body>

</html>

<!-- <div id="#monid"></div> -->
<!-- $("#monid").append("moniframe") -->

<!-- var id = document.getelementbyid(monid)
      id. -->
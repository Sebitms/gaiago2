<?php
// on verifie si l'ultilsateur est un admin
  session_start();
  if ($_SESSION['login'] != TRUE){
      header('Location: admin.php');
      exit;
  }
  // on fait le traitement necessaire pour ecrire dans le placeholder de la video
  include "../../inc/database.php";
  $dbh = $result->query('SELECT Adresse FROM iframe WHERE ID = "1"');
  foreach($dbh as $row){
    $explode1 = explode('/',$row[0]);
    $explode2 = explode('"',$explode1[4]);
    $iframe = $explode2[0];
  }
   // on regarde si la video et actif ou non
  $dbh = $result->query('SELECT `Titre`,`Actif` FROM `page` WHERE `Titre` = "Free N 100" ');
  foreach($dbh as $row){
?>
<!-- Status de la page selon la lecture de la base de donnée -->
<h2 class="alert alert-success" id="desactivefreen100" <?php  if ($row['Actif'] == 0){ echo "style='display: none;'";} ?> >Landing page actuellement en ligne</h2>
<h2 class="alert alert-danger" id="activefreen100" <?php  if ($row['Actif'] == 1){ echo "style='display: none;'";} ?> >Landing page désactivé !</h2>

<!-- Affichage du bon bouton selon la lecture de la base de donnée -->
<button id='desactive' name='Free N 100' type='button' class='btn btn-info' <?php  if ($row['Actif'] == 0){ echo "style='display: none;'";} ?> >Désactivé la page</button>
<button id='active' name='Free N 100' type='button' class='btn btn-info' <?php  if ($row['Actif'] == 1){ echo "style='display: none;'";} ?>>Activé la page</button>

  <?php } ?>
  <hr class="hr-bottom-4em">

<div class="jumbotron">
  <div class="container">
  <h1 class="display-1">Free N100</h1>
  </div>
</div>

<div class="jumbotron jumb-blue">
  <div class="container">
    <h2>Vidéo</h2>
<!-- L'id doit correspondre a iframe  + nom du document (document.title) -->
    <div class="iframe" id="iframefreen100">
        
    </div>
    
  <section class="new-ifram-php">
    <small>* Vidéo actuellement sur la page produit FreeN100</small>

    <h3>Intégrer la vidéo ici </h3>

    
    <div class="jumbotron">

      <a href="https://support.google.com/youtube/answer/171780?hl=fr" target="_blank" rel="noopener">comment faire ?</a>
      <br><br><p>Mettez votre lien https ici !</p>
      <input class="form-control" type="text" id="newfree" placeholder="Adresse actuelle : https://youtube.com/watch?v=<?php echo $iframe?>" >
      
    </div>
    
    <button class="button-ifram btn btn-success" type="submit" id="envoyerfree">Sauver</button>
    <p id="resultatfree"></p>

    </div>
    </div>
  </section> 
    <!-- Formualire d'envoi d'image vers dossier image -->
  
    <hr class="hr-bottom-4em">


  <div class="jumbotron jumb-blue">
  <div class="container">

      <div class="jumbotron jumb-white">
        <h2>Mon image d'en tête</h2>
        <p>Pour changer d'image sur la page FreeN100 c'est par ici ! </p>
        <!-- <img class="image-dash" src="../image/monitor-313011_640.jpg" alt="image"> -->
      </div>
<div class="jumbotron">
<form enctype="multipart/form-data" action="html-dashboard/upload.php" method="POST">
 
  <div class="custom-file">
     <!-- On limite le fichier à 10000Ko -->
     
     <input type="hidden" name="MAX" value="10000000">
     Fichier : <input class="custom-file-input" value="free" type="file" name="update" aria-describedby="inputGroupFileAddon01">
     <label class="custom-file-label" for="inputGroupFile01">Choisissez un fichier de type JPG</label>
    
     <!-- C'est un code triche ne pas toucher !!!!!! -->
     <input type="text" name="name[header-freen100]" style="display:none">
    
     <!-- Ne pas toucher la ligne du dessus !!!!!! -->
     
     <input class="btn btn-info" type="submit" name="envoyer" value="Envoyer le fichier">
     <p>Pour le bien-être du site, évitez de mettre des images supérieure à 1Mo.</p>
     <hr class="hr-bottom-4em">

     <a href="https://image.online-convert.com/fr/convertir-en-jpg" target="_blank" rel="noopener">Comment convertir une image ! </a>

    
  </div>
</form>
    </div>
</div>
</div>


<!-- permet de modifier les chiffre gaiago -->
<div class="jumbotron jumb-blue">
  <div class="container">

  </div>
<div class="jumbotron">
<h2>Changez le fonctionement de la calculette</h1>
<select id="choixculture" class="culture-input form-control" >
    <option disabled selectec>Veuillez choisir votre culture</option>
    <option disabled >Grandes Cultures :</option>
    <option value="blé-cal">Blé</option>
    <option value="maïs-cal">Maïs</option>
    <option value="soja-cal">Soja</option>
    <option value="tournesol-cal">Tournesol</option>
    <option value="orge-cal"> Orge</option>
    <option value="tritical-cal">Tritical</option>
    <option disabled>Cultures industrielles :</option>
    <option value="colza-cal">Colza</option>
    <option value="pommeDeTerre-cal">Pommes de Terre</option>
    <option value="carotteLeg-cal">Carottes Légumes</option>
    <option value="culturesFruit-cal">Cultures fruitières</option>
    <option value="culturesOrme-cal">Cultures ornementales</option>
  </select>

  <!-- Les 4 prochaine ligne doit avoir la classe hidden(non utilisé dans le css, conctaté Seb en cas de changement) -->

  <p class="hidden" id="textcalc" style="display:none"></p>
  <input class="hidden" id="choixcalc" type="number" style="display:none" require>
  <small class="ast-cal hidden" style="display:none">* Vous pouvez changer le chiffre directement ici </small>
  <button class="btn btn-info hidden" id="change" style="display:none">Changer le chiffre</button>
  <p id="resultchange"></p>

</div>


<script>
  $('#envoyerfree').click(function (){
    $.ajax({
      url : 'html-dashboard/script.php',
      type : 'post',
      data : {adresse: $('#newfree').val(), dbh : "1"},
      datatype: 'JSON',
      success : function(rsp){
          var reponse = JSON.parse(rsp)
          if (reponse['status'] == "ok"){
              $('#resultatfree').html("L'adresse a bien été enregistré")
          } else if(reponse['status'] == "Bad link"){
              $('#resultatfree').html("Merci de rentrer un lien Youtube")
          }
      },
      error : function(){
          $('#resultatfree').html("L'adresse n'a pas correctement été enregistré")
      }
    });
  })

  $('#desactive').click(function(){
    var name = $('#desactive').attr("name")
    var choice = "0"
    $.ajax({
      url : 'html-dashboard/script.php',
      type : 'post',
      data : {nom : name, choix : choice },
      datatype: 'JSON',
      success : function(rsp){
        $('#desactive').css("display", "none")
        $('#desactivefreen100').css("display", "none")
        $('#active').css("display", "block")
        $('#activefreen100').css("display", "block")
      },
      error : function(){
          
      }
    });
  })

  $('#active').click(function(){
    var name = $('#active').attr("name")
    var choice = "1"
    $.ajax({
      url : 'html-dashboard/script.php',
      type : 'post',
      data : {nom : name, choix : choice },
      datatype: 'JSON',
      success : function(rsp){
        $('#desactive').css("display", "block")
        $('#desactivefreen100').css("display", "block")
        $('#active').css("display", "none")
        $('#activefreen100').css("display", "none")
      },
      error : function(){
          
      }
    });
  })

  $('#choixculture').click(function(){
    var culture = $('#choixculture option:selected').text();
      $.ajax({
        url : 'html-dashboard/script.php',
        type : 'post',
        data : {appel : culture },
        datatype: 'JSON',
        success : function(rsp){
          var resultat = JSON.parse(rsp)
          $('#textcalc').html("La calculette utilise ce nombre ( en pourcentage ) pour faire son calcul : ")
          $('#choixcalc').attr("placeholder", resultat['result'])
          $('.hidden').css("display", "block")
        },
        error : function(){
            
        }
      });
    })

    $('#change').click(function(){
  var culture = $('#choixculture option:selected').text();
  var newnumber = $('#choixcalc').val();
  if(newnumber === ""){
    $('#resultchange').html("Merci de rentrer un chiffre valide dans le champ de saisie au dessus! ")
  }else{
    $.ajax({
      url : 'html-dashboard/script.php',
      type : 'post',
      data : {number : newnumber, choice : culture },
      success : function(){
       $('#resultchange').html("La culture " + culture + " a bien été changé a la valeur " + newnumber)
      },
      error : function(){
          
      }
      });
    }
    })
</script>
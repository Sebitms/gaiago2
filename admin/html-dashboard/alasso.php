<?php
// on verifie si l'ultilsateur est un admin
  session_start();
  if ($_SESSION['login'] != TRUE){
      header('Location: admin.php');
      exit;
  }
 // on regarde si la page est actif ou non
 include "../../inc/database.php";
  $dbh = $result->query('SELECT `Titre`,`Actif` FROM `page` WHERE `Titre` = "Alasso" ');
  foreach($dbh as $row){
?>
<!-- Status de la page selon la lecture de la base de donnée -->
<h2 class="alert alert-success" id="desactivealasso" <?php  if ($row['Actif'] == 0){ echo "style='display: none;'";} ?> >Landing page actuellement en ligne</h2>
<h2 class="alert alert-danger" id="activealasso" <?php  if ($row['Actif'] == 1){ echo "style='display: none;'";} ?> >Landing page désactivé !</h2>
<button id='desactive' name='Alasso' type='button' class='btn btn-info' <?php  if ($row['Actif'] == 0){ echo "style='display: none;'";} ?> >Désactivé la page</button>
<button id='active' name='Alasso' type='button' class='btn btn-info' <?php  if ($row['Actif'] == 1){ echo "style='display: none;'";} ?>>Activé la page</button>
<?php } ?>
<hr>


<div class="jumbotron">

    <div class="container">

      <h1 class="display-1">Alasso</h1>
      <p>Est en construction et donc pas accessible pour le moment, merci de vôtre compréhension.</p>

    </div>  
  </div>

<!-- Formualire d'envoi d'image vers dossier image -->


<div class="jumbotron jumb-blue">
  <div class="container">

      <div class="jumbotron jumb-white">
        <h2>Mon image d'en tête</h2>
        <p>Pour changer d'image sur la page Nutrigéo c'est par ici !</p>
        <!-- <img class="image-dash" src="../image/monitor-313011_640.jpg" alt="image"> -->
      </div>

<!-- Formualire d'envoi d'image vers dossier image -->
<div class="jumbotron">
<form enctype="multipart/form-data" action="html-dashboard/upload.php" method="POST">
  <div class="custom-file">
     <!-- On limite le fichier à 10000Ko -->
     <input type="hidden" name="MAX" value="10000000">
     Fichier : <input class="custom-file-input" value="free" type="file" name="update" aria-describedby="inputGroupFileAddon01">
     <label class="custom-file-label" for="inputGroupFile01">Choisissez un fichier de type JPG</label>
     
     <!-- C'est un code triche ne pas toucher !!!!!! -->
     <input type="text" name="name[header-alasso]" style="display:none">
     <!-- Ne pas toucher la ligne du dessus !!!!!! -->
     
     <input class="btn btn-info" type="submit" name="envoyer" value="Envoyer le fichier">
  </div>
</form>
</div>
</div>
</div>


<script>

$('#desactive').click(function(){
    var name = $('#desactive').attr("name")
    var choice = "0"
    $.ajax({
        url : 'html-dashboard/script.php',
        type : 'post',
        data : {nom : name, choix : choice },
        datatype: 'JSON',
        success : function(rsp){
          $('#desactive').css("display", "none")
          $('#desactivealasso').css("display", "none")
          $('#active').css("display", "block")
          $('#activealasso').css("display", "block")
        },
        error : function(){
           
        }
    });
  })

  $('#active').click(function(){
    var name = $('#active').attr("name")
    var choice = "1"
    $.ajax({
        url : 'html-dashboard/script.php',
        type : 'post',
        data : {nom : name, choix : choice },
        datatype: 'JSON',
        success : function(rsp){
          $('#desactive').css("display", "block")
          $('#desactivealasso').css("display", "block")
          $('#active').css("display", "none")
          $('#activealasso').css("display", "none")
        },
        error : function(){
           
        }
    });
  })
  </script>
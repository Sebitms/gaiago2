<?php
    session_start();
    if ($_SESSION['login'] != TRUE){
        header('Location:../index.php');
        exit;
    }

    include "../../inc/database.php";
// mis a jour automatique des point de ventes
if (isset($_POST['autoupdate'])){ 
    // Ici on ouvre directement le fichier google doc
    $filename = fopen('https://docs.google.com/spreadsheets/d/1RVvJ-sgwbJMSVMZOzLVTOc7vPvaO2sQrKATNygJTWLo/export?format=csv&id=1RVvJ-sgwbJMSVMZOzLVTOc7vPvaO2sQrKATNygJTWLo&gid=0', "r");

    // On traite les information recuperer en les convertissant sous le format csv
    while (($column = fgetcsv($filename, 10000, ",")) !== FALSE) {
        $code = substr($column[8], 0, 4);
        $code .= "0";
    //on envoie chaque ligne traités vers la base de données
        $dbh = $result->prepare("INSERT INTO `point de ventes` (`2020`,`Nom`,`Adresse`, `Adresse2`, `Telephone`, `Code_ville`) 
                                VALUES ( ? ,? ,? ,? ,? ,?) ");
        $dbh->execute(array($column[5], $column[0],$column[7], $column[8], $column[9], $code));

        if (! empty($result)) {
            $type = "success";
            $message = "Les Données sont importées dans la base de données";
          } else {
            $type = "error";
            $message = "Problème lors de l'importation de données CSV";
          }
    }
    exit;
}
// mis a jour manuel des point de ventes
if (isset($_POST['MAX_FILE_SIZE'])){
    $fichier = basename($_FILES['manualupdate']['name']);
    $taille_maxi = 1000000;
    $taille = filesize($_FILES['manualupdate']['tmp_name']);
    $extensions = array('.csv');
    $extension = strrchr($_FILES['manualupdate']['name'], '.'); 
    //Début des vérifications de sécurité...
    if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
    {
        $erreur = 'Vous devez uploader un fichier de type .csv';
        header('location:../dashboard.php?error=type');
        exit;
    }
    if($taille>$taille_maxi)
    {
        $erreur = 'Le fichier est trop gros...';
        header('location:../dashboard.php?error=size');
        exit;
    }
    if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
    {
        //On formate le nom du fichier ici...
        $fichier = strtr($fichier, 
            'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 
            'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
        $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

        $fileName = $_FILES["manualupdate"]["tmp_name"];

        if ($_FILES["manualupdate"]["size"] > 0) {

            $file = fopen($fileName, "r");

            $dbh = $result->query("TRUNCATE `point de ventes`");

            while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
                $code = substr($column[8], 0, 4);
                $code .= "0";
                $dbh = $result->prepare("INSERT INTO `point de ventes` (`2020`,`Nom`,`Adresse`, `Adresse2`, `Telephone`, `Code_ville`) 
                                VALUES ( ? ,? ,? ,? ,? ,?) ");
                $dbh->execute(array($column[5], $column[0],$column[7], $column[8], $column[9], $code));

            if (! empty($result)) {
            $type = "success";
            $message = "Les Données sont importées dans la base de données";
            } else {
            $type = "error";
            $message = "Problème lors de l'importation de données CSV";
            }
            }
        }
    }
    header('location:../dashboard.php');
    exit;
}
// remplace les image d'en tete des pages produits
if (isset($_POST['MAX'])){
    $name = key($_POST['name']);
    $fichier = basename($_FILES['update']['name']);
    $taille_maxi = 10000000;
    $taille = filesize($_FILES['update']['tmp_name']);
    $extensions = array('.jpg');
    $extension = strrchr($_FILES['update']['name'], '.'); 
    //Début des vérifications de sécurité...
    if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
    {
        // $erreur = 'Vous devez uploader un fichier de type .jpg';
        // header('location:../dashboard.php?error=type');
        // exit;
    }
    if($taille>$taille_maxi)
    {
        // $erreur = 'Le fichier est trop gros...';
        // header('location:../dashboard.php?error=size');
        // exit;
    }
    if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
    {
        $fileName = $_FILES["update"]["tmp_name"];


        unlink ("../../image/$name.jpg");
        move_uploaded_file ($fileName , "../../image/$name.jpg" );
     
    
    }
    header('location:../dashboard.php');
    exit;
    if (isset($erreur)){
        var_dump($erreur);
    }
}
?>
<?php
// on verifie si l'ultilsateur est un admin
  session_start();
  if ($_SESSION['login'] != TRUE){
      header('Location: admin.php');
      exit;
  }
  // on fait le traitement necessaire pour ecrire dans le placeholder de la video
  include "../../inc/database.php";
  $dbh = $result->query('SELECT Adresse FROM iframe WHERE ID = "2"');
  foreach($dbh as $row){
    $explode1 = explode('/',$row[0]);
    $explode2 = explode('"',$explode1[4]);
    $iframe = $explode2[0];
  }
  // on regarde si la video et actif ou non

  $dbh = $result->query('SELECT `Titre`,`Actif` FROM `page` WHERE `Titre` = "Nutrigeo" ');
  foreach($dbh as $row){
?>
<!-- Status de la page selon la lecture de la base de donnée -->

<h2 class="alert alert-success" id="desactivenutrigeo" <?php  if ($row['Actif'] == 0){ echo "style='display: none;'";} ?> >Landing page actuellement en ligne</h2>
<h2 class="alert alert-danger" id="activenutrigeo" <?php  if ($row['Actif'] == 1){ echo "style='display: none;'";} ?> >Landing page désactivé !</h2>

<!-- Affichage du bon bouton selon la lecture de la base de donnée -->

<button id='desactive' name='Nutrigéo' type='button' class='btn btn-info' <?php  if ($row['Actif'] == 0){ echo "style='display: none;'";} ?> >Désactivé la page</button>
<button id='active' name='Nutrigéo' type='button' class='btn btn-info' <?php  if ($row['Actif'] == 1){ echo "style='display: none;'";} ?>>Activé la page</button>
<?php } ?>

<hr class="hr-bottom-4em">

<div class="jumbotron">
  <div class="container">
  <h1 class="display-1">Nutrigéo</h1>
  </div>
</div>


<div class="jumbotron jumb-blue">
  <div class="container">
    <h2>Vidéo</h2>
    <!-- L'id doit correspondre a iframe  + nom du document (document.title) -->
    <div class="iframe" id="iframenutrigeo">
      
    </div>

  <section class="new-ifram-php">
    <small>* Vidéo actuellement sur la page produit Nutrigeo</small>
  
    <h3>Intégrer la vidéo ici</h3>

    <div class="jumbotron">
  
      <a href="https://support.google.com/youtube/answer/171780?hl=fr" target="_blank" rel="noopener">comment faire ?</a>
      <br><br><p>Mettez votre lien https ici !</p>
      <input class="form-control" type="text" id="newnutri" placeholder="Adresse actuelle : https://youtube.com/watch?v=<?php echo $iframe?>">

    </div>

    <button class="button-ifram btn btn-success" type="submit" id="envoyernutri">Sauver</button>
    <p class="alert-danger" role="alert" id="resultatnutri"></p>

  </div>
  </div>
  </section>

  <hr class="hr-bottom-4em">

<div class="jumbotron jumb-blue">
  <div class="container">

      <div class="jumbotron jumb-white">
        <h2>Mon image d'en tête</h2>
        <p>Pour changer d'image sur la page Nutrigéo c'est par ici !</p>
        <!-- <img class="image-dash" src="../image/monitor-313011_640.jpg" alt="image"> -->
      </div>

<!-- Formualire d'envoi d'image vers dossier image -->
<div class="jumbotron">
<form enctype="multipart/form-data" action="html-dashboard/upload.php" method="POST">
  <div class="custom-file">
     <!-- On limite le fichier à 10000Ko -->
     <input type="hidden" name="MAX" value="10000000">
     Fichier : <input class="custom-file-input" value="free" type="file" name="update" aria-describedby="inputGroupFileAddon01">
     <label class="custom-file-label" for="inputGroupFile01">Choisissez un fichier de type JPG</label>
     
     <!-- C'est un code triche ne pas toucher !!!!!! -->
     <input type="text" name="name[header-nutrigeo]" style="display:none">
     <!-- Ne pas toucher la ligne du dessus !!!!!! -->
     
     <input class="btn btn-info" type="submit" name="envoyer" value="Envoyer le fichier">
     <p>Pour le bien-être du site, évitez de mettre des images supérieure à 1Mo.</p>
     <hr class="hr-bottom-4em">

     <a href="https://image.online-convert.com/fr/convertir-en-jpg" target="_blank" rel="noopener">Comment convertir une image ! </a>

  </div>
</form>
</div>
</div>
</div>
  
    <script>
      $('#envoyernutri').click(function(){
      $.ajax({
        url : 'html-dashboard/script.php',
        type : 'post',
        data : {adresse: $('#newnutri').val(), dbh : "2"},
        datatype: 'JSON',
        success : function(rsp){
            var reponse = JSON.parse(rsp)
            if (reponse['status'] == "ok"){
                $('#resultatnutri').html("L'adresse a bien été enregistré")
            } else if(reponse['status'] == "Bad link"){
              // window.alert("Merci de rentrer un lien Youtubettryrtyrtytr")
                $('#resultatnutri').html("Je suis désolé, mais votre demande n'a pu être prise en compte. Merci de rentrer un lien Youtube. ( https://www.youtube.com/ ) ")
            }
        },
        error : function(){
            $('#resultatnutri').html("L'adresse n'a pas correctement été enregistré")
        }
      });
    })

    $('#desactive').click(function(){
    var name = $('#desactive').attr("name")
    var choice = "0"
    $.ajax({
        url : 'html-dashboard/script.php',
        type : 'post',
        data : {nom : name, choix : choice },
        datatype: 'JSON',
        success : function(rsp){
          $('#desactive').css("display", "none")
          $('#desactivenutrigeo').css("display", "none")
          $('#active').css("display", "block")
          $('#activenutrigeo').css("display", "block")
        },
        error : function(){
           
        }
    });
  })

  $('#active').click(function(){
    var name = $('#active').attr("name")
    var choice = "1"
    $.ajax({
        url : 'html-dashboard/script.php',
        type : 'post',
        data : {nom : name, choix : choice },
        datatype: 'JSON',
        success : function(rsp){
          $('#desactive').css("display", "block")
          $('#desactivenutrigeo').css("display", "block")
          $('#active').css("display", "none")
          $('#activenutrigeo').css("display", "none")
        },
        error : function(){
           
        }
    });
  })
    </script>
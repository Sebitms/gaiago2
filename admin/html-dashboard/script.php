<?php
    session_start();
    if ($_SESSION['login'] != TRUE){
        header('Location: admin.php');
        exit;
    }

    include "../../inc/database.php";
// met a jour l'iframe des video youtube
    if (isset($_POST['adresse'])){
       $id = $_POST['adresse'];
       $dbh = $_POST['dbh'];
        if (strpos($id, "https://www.youtube.com/") !== FALSE) {
            if (strpos($id, "embed") === FALSE) {
                $explode1 = explode('/',$id);
                $explode2 = explode('&',$explode1[3]);
                $explode3 = explode('=',$explode2[0]);
                $id = '<iframe width="560" height="315" src="https://www.youtube.com/embed/' . $explode3[1] . '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                } 

                if ($dbh == "1"){
                    $dbh = $result->prepare("UPDATE `iframe` SET `Adresse` = ? WHERE `ID` = '1'");
                    $dbh->execute(array($id));
                    
                } else {
                    $dbh = $result->prepare("UPDATE `iframe` SET `Adresse` = ? WHERE `ID` = '2'");
                    $dbh->execute(array($id));
                }
                echo json_encode([
                "status" => "ok"]);
                exit;
        } else {
            echo json_encode([
            "status" => "Bad link"]);
        }
    }
   
  // Permet de recuperer l'iframe youtube
    if (isset($_POST['code'])){
        $id = $_POST['code'];
        if ($id == "freen100"){
            $code = 1;
        } else if ($id == "nutrigeo") {
            $code = 2;
        }
        $dbh = $result->prepare('SELECT Adresse FROM iframe WHERE ID = ? ');
        $dbh->execute(array($code));
        foreach($dbh as $row){
            echo $row[0];
        }
        exit;
    }
    // active ou desactive les pages produits
    if (isset($_POST['nom'])){
        $nom = $_POST['nom'];
        $choix = $_POST['choix'];
        $dbh = $result->prepare('UPDATE `page` SET `Actif` = ? WHERE `page`.`Titre` = ? ');
        $dbh->execute(array($choix, $nom));
    }
// Montre les données relatif  a la carte presente dans la base de données
    if (isset($_POST['showdata'])){   
        $data = array();
        $dbh = $result->query('SELECT `Nom`,`Adresse`,`Adresse2`,`Telephone` FROM `point de ventes` WHERE `2020` ="x" ');
        foreach($dbh as $row){
            array_push($data, array("Nom" => $row['Nom'],"Adresse" => $row['Adresse'],"Adresse2" => $row['Adresse2'],"Telephone"=>$row['Telephone']));
        }
        $json = json_encode($data,JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    // Montre les données relatif  a la calulatrice presente dans la base de données
if(isset($_POST['appel'])){
    $culture = $_POST['appel'];
    $dbh = $result->prepare('SELECT `Culture`,`Chiffre` FROM `calculatrice` WHERE `Culture` = ? ');
    $dbh->execute(array($culture));
    foreach ($dbh as $row){
        $result = $row['Chiffre'];
    }
    echo json_encode([
        "result" => $result
        ]);

}

// Met a jour la calculette
if (isset($_POST['number'])){
    $culture = $_POST['choice'];
    $number = $_POST['number'];
    $dbh = $result->prepare('UPDATE `calculatrice` SET `Chiffre` = ? WHERE `Culture` = ? ');
    $dbh->execute(array($number, $culture));
}
?>
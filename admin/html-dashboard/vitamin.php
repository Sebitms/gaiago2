<?php
// on verifie si l'ultilsateur est un admin
    session_start();
    if ($_SESSION['login'] != TRUE){
        header('Location: admin.php');
        exit;
    }
     // on regarde si la video et actif ou non
     include "../../inc/database.php";
    $dbh = $result->query('SELECT `Titre`,`Actif` FROM `page` WHERE `Titre` = "Vitamin" ');
    foreach($dbh as $row){
  ?>
  <!-- Status de la page selon la lecture de la base de donnée -->
  <h2 class="alert alert-success" id="desactivevitamin" <?php  if ($row['Actif'] == 0){ echo "style='display: none;'";} ?> >Landing page actuellement en ligne</h2>
  <h2 class="alert alert-danger"  id="activevitamin" <?php  if ($row['Actif'] == 1){ echo "style='display: none;'";} ?> >Landing page désactivé !</h2>

<!-- Affichage du bon bouton selon la lecture de la base de donnée -->

  <button id='desactive' name='Vitamin' type='button' class='btn btn-info' <?php  if ($row['Actif'] == 0){ echo "style='display: none;'";} ?> >Désactivé la page</button>
  <button id='active' name='Vitamin' type='button' class='btn btn-info' <?php  if ($row['Actif'] == 1){ echo "style='display: none;'";} ?>>Activé la page</button>



<?php } ?>


<hr class="hr-bottom-4em">

<div class="jumbotron">

    <div class="container">

      <h1 class="display-1">Vitam'in</h1>
      
    </div>  
  </div>

<!-- Formualire d'envoi d'image vers dossier image -->


<div class="jumbotron jumb-blue">
  <div class="container">

      <div class="jumbotron jumb-white">
        <h2>Mon image d'en tête</h2>
        <p>Pour changer d'image sur la page stimulus c'est par ici !</p>
        <!-- <img class="image-dash" src="../image/monitor-313011_640.jpg" alt="image"> -->
      </div>

<!-- Formualire d'envoi d'image vers dossier image -->
<div class="jumbotron">
<form enctype="multipart/form-data" action="html-dashboard/upload.php" method="POST">
  <div class="custom-file">
     <!-- On limite le fichier à 10000Ko -->
     <input type="hidden" name="MAX" value="10000000">
     Fichier : <input class="custom-file-input" value="free" type="file" name="update" aria-describedby="inputGroupFileAddon01">
     <label class="custom-file-label" for="inputGroupFile01">Choisissez un fichier de type JPG</label>
     
     <!-- C'est un code triche ne pas toucher !!!!!! -->
     <input type="text" name="name[header-vitam]" style="display:none">
     <!-- Ne pas toucher la ligne du dessus !!!!!! -->
     
     <input class="btn btn-info" type="submit" name="envoyer" value="Envoyer le fichier">
  </div>
</form>
</div>
</div>
</div>
      
<script>

$('#desactive').click(function(){
    var name = $('#desactive').attr("name")
    var choice = "0"
    $.ajax({
        url : 'html-dashboard/script.php',
        type : 'post',
        data : {nom : name, choix : choice },
        datatype: 'JSON',
        success : function(rsp){
          $('#desactive').css("display", "none")
          $('#desactivevitamin').css("display", "none")
          $('#active').css("display", "block")
          $('#activevitamin').css("display", "block")
        },
        error : function(){
           
        }
    });
  })

  $('#active').click(function(){
    var name = $('#active').attr("name")
    var choice = "1"
    $.ajax({
        url : 'html-dashboard/script.php',
        type : 'post',
        data : {nom : name, choix : choice },
        datatype: 'JSON',
        success : function(rsp){
          $('#desactive').css("display", "block")
          $('#desactivevitamin').css("display", "block")
          $('#active').css("display", "none")
          $('#activevitamin').css("display", "none")
        },
        error : function(){
           
        }
    });
  })
  </script>
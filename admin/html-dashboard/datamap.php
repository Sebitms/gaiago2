<?php
    session_start();
    if ($_SESSION['login'] != TRUE){
        header('Location: admin.php');
        exit;
    }

// on regarde si la video et actif ou non
include "../../inc/database.php";
$dbh = $result->query('SELECT `Titre`,`Actif` FROM `page` WHERE `Titre` = "map" ');
foreach($dbh as $row){
?>
<!-- Status de la page selon la lecture de la base de donnée -->

<h2 class="alert alert-success" id="desactivemap" <?php  if ($row['Actif'] == 0){ echo "style='display: none;'";} ?> >Map actuellement en ligne</h2>
<h2 class="alert alert-danger" id="activemap" <?php  if ($row['Actif'] == 1){ echo "style='display: none;'";} ?> >Map désactivé !</h2>
<!-- Affichage du bon bouton selon la lecture de la base de donnée -->

<button id='desactive' name='map' type='button' class='btn btn-info' <?php  if ($row['Actif'] == 0){ echo "style='display: none;'";} ?> >Désactivé la page</button>
<button id='active' name='map' type='button' class='btn btn-info' <?php  if ($row['Actif'] == 1){ echo "style='display: none;'";} ?>>Activé la page</button>

<?php } ?>

<div class="container">
<div class="jumbotron">
<h1 class="display-3">Map et base de donnée </h1>
<p>Mettre à jour les points de ventes Nutrigeo : </p>

<button class="btn btn-primary" type="button" id="autoupdate">Mettre a jour</button>
<p>Voici l'adresse actuelle de la liste des points de ventes : 
"https://docs.google.com/spreadsheets/d/1Hd77bwjposoocfjrTeLMTMgUITwS3xADa5573tHoVwQ/edit#gid=1043121043"
</p>
<p id="resultatcsv">L'opération peut prendre jusqu'a 2 min</p>
</div>
</div>
<hr class="hr-bottom-4em">

<div class="container">
<div class="jumbotron">

<p>Si l'adresse Google sheet à changer ou est inutilisable, Veuillez envoyez vos données ici</p>
<br>
<a href="https://captainverify.com/fr/csv.html" target="_blank rel="nooponer> Voir comment faire ici</a>
<hr class="hr-bottom-4em">


<!-- Formualire d'envoi CSV vers base de données -->
<form enctype="multipart/form-data" action="html-dashboard/upload.php" method="POST">
  <div class="custom-file">
     <!-- On limite le fichier à 10000Ko -->
     <input type="hidden" name="MAX_FILE_SIZE" value="10000000">
     Fichier : <input class="custom-file-input" type="file" name="manualupdate" aria-describedby="inputGroupFileAddon01">
     <label class="custom-file-label" for="inputGroupFile01">Choisissez un fichier de type CSV</label>
     <input class="btn btn-info" type="submit" name="envoyer" value="Envoyer le fichier">
     
  </div>
</form>

</div>
</div>

<hr class="hr-bottom-4em">

<div class="container">
<div class="jumbotron">

<!-- recupere la data sur la base de donnée et affiche un tabelaux le contenant -->

<button class="btn btn-info" id="showmedata">Voir les données présente dans la base de données</button>
<div id="hereyourdata" style="display : none">
<small class="ast-cal">* Ne montre que les résultat qui seront utilisé pour mettre a jour dans les maps</small>
<table >
  <thead>
    <tr>
      <th>Nom du distributeur</th>
      <th>Adresse</th>
      <th>Adresse2</th>
      <th>Telephone</th>
    </tr>
  </thead>
    <tbody id="here">
    </tbody>
  </thead>
</table>
</div>

</div>
</div>

<script>

$('#autoupdate').click(function(){
      $.ajax({
        url : 'html-dashboard/upload.php',
        type : 'post',
        data : {autoupdate : ""},
        datatype: 'JSON',
        beforeSend: function () {
            $('#autoupdate').html('Chargement...')
            $('#autoupdate').prepend("<span id='loading' class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span>")
          },
        success : function(){
            $('#autoupdate').html('Correctement mis a jour')
        },
        error : function(){
            $('#resultatcsv').html("Il y a eu une erreur pendant la mise a jour merci de rééssayé plus tard")
        }
      });
    })

  $('#showmedata').click(function(){
    $.ajax({
        url : 'html-dashboard/script.php',
        type : 'post',
        data : {showdata : ""},
        datatype: 'JSON',
        success : function(rsp){
            var result = JSON.parse(rsp)
            $('#hereyourdata').css("display","block")
            for (i = 0 ; i < result.length; i++){
            $('#here').append("<tr>"+
                                "<th>"+ result[i]['Nom'] + "</th><br>" +
                                "<th>"+ result[i]['Adresse'] + "</th><br>" +
                                "<th>"+ result[i]['Adresse2'] + "</th><br>" +
                                "<th>"+ result[i]['Telephone'] + "</th>"+
                              "</tr>")
            }
            $("#showmedata").prop('disabled',true)
        },
        error : function(){

        }
      });
    })

  $('#desactive').click(function(){
  var name = $('#desactive').attr("name")
  var choice = "0"
  $.ajax({
      url : 'html-dashboard/script.php',
      type : 'post',
      data : {nom : name, choix : choice },
      datatype: 'JSON',
      success : function(rsp){
        $('#desactive').css("display", "none")
        $('#desactivemap').css("display", "none")
        $('#active').css("display", "block")
        $('#activemap').css("display", "block")
      },
      error : function(){
          
      }
  });
})

$('#active').click(function(){
  var name = $('#active').attr("name")
  var choice = "1"
  $.ajax({
      url : 'html-dashboard/script.php',
      type : 'post',
      data : {nom : name, choix : choice },
      datatype: 'JSON',
      success : function(rsp){
        $('#desactive').css("display", "block")
        $('#desactivemap').css("display", "block")
        $('#active').css("display", "none")
        $('#activemap').css("display", "none")
      },
      error : function(){
          
      }
  });
})

  </script>
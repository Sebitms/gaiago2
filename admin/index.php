<!doctype html>
<html lang="fr">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link  rel="icon"  href="../image/favicon.ico"  type="image/x-icon">
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen&display=swap" rel="stylesheet">

    <!-- style css -->

    <!-- Bootstrap CSS -->
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/main.css">

</head>

<body class="body-admin">
    <!-- Partie Header -->

    <header class="header-admin">

          <ul class="nav justify-content-center nav-admin">
            <li class="nav-item">
              <a class="nav-link active" href="../index.php">Retour au Produit</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="https://gaiago.eu/" target="_blank" rel="noopener">Home</a>
            </li>
          </ul>

    </header>

    <h1 class="text-admin">Administration</h1>
    
    <form class="center_admin" action="login.php" method="POST">
      
      <?php
        session_start();
          if (isset ($_SESSION['login'])){
            echo "<p class='alert alert-danger' role='alert'>Votre identifiant ou mot de passe sont incorrect</p>";
            $_SESSION = array(); 
            session_destroy (); 
          }
      ?>
      
        <div class="form-group">
            <label for="exampleInputEmail1">Nom d'utilisateur</label>
            <input type="text" name="nom" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            <small id="emailHelp" class="form-text text-muted">Veuillez entrez votre nom d'utilisateur</small>
          </div>
            <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1">
        </div>
      <button id="btn-admin" type="submit" class="btn btn-dark ">Se connecter</button>
      <?php
      if (isset($_SESSION['login'])){
        if($_SESSION['login'] == TRUE){
          header('Location: dasboard.php');
          exit;
        } else{
          echo "<p class='alert alert-danger pictos-title' role='alert'>Identifiant incorrect</p>";
          $_SESSION = array(); 
          session_destroy (); 
        }
      }
      ?>

    </form>

    <!-- Partie Footer -->
  
  </body>

</html>
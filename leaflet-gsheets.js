// Valeur de base pour afficher la map
var latittude = 46.227638;
var longitude = 2.213749;
var zoom = 5;
var map = undefined;
// Selection de la l'adresse
$('#formmap').submit(function (event){
  event.preventDefault();
  $.ajax({
    url : 'data/call.php',
    type : 'post',
    data : {adresse : $('#codepostal').val()},
    datatype: 'JSON',
    success : function(rsp){
      var result = JSON.parse(rsp)
      console.log(result['latitude'])
      console.log(result['longitude'])
      if (result['code'] == 'TRUE'){
        L.marker([result['latitude'], result['longitude']]).addTo(map)
        .bindPopup("Désolé ! Nous n'avons pas de point de vente à proximité de chez vous... Cependant des distributeurs nous reférencent chaques jours, n'hésitez pas à nous appeler au 02 99 88 73 91 pour connaître nos points de vente les plus proches")
        .openPopup();
      } else if (result['code'] == 'WRONG'){
        $('#resultfail').html('Merci de rentrer un code postal ou départementale correcte')
      }
      map.flyTo([result['latitude'], result['longitude']], 12);
      
      // map.openPopup([result['latitude'], result['longitude']]);
    },
    error : function(){
    }
});
})
// Create a new Leaflet map centered on the continental US
var map = L.map("map").setView([latittude, longitude], zoom);

// This is the Carto Positron basemap

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

point();
function point(){
  $.ajax({
    url : 'data/call.php',
    type : 'post',
    data : {point : ""},
    datatype: 'JSON',
    success : function(rsp){
      var resultpoint = JSON.parse(rsp)
      var length = Object.keys(resultpoint).length
      for (i=0;i<length;i++){
        var pointnom =resultpoint[i]['Nom'];
        var pointlongitude = resultpoint[i]['longitude'];
        var pointlatitude = resultpoint[i]['latitude'];
        var pointtelephone = resultpoint[i]['Telephone'];
        L.marker([pointlatitude, pointlongitude], {icon : greenIcon}).addTo(map)
        .bindPopup("Nom du distributeur : " + pointnom +"<br>Telephone : "+ pointtelephone +"<br>Retrouvez nous dans le dépôt le plus proche !")
      
      }
    },
    error : function(){

    }
});

}
var greenIcon = L.icon({
  iconUrl: 'image/gaiago26px.png',
  iconSize:[26, 26], // size of the icon
});
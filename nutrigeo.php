<?php
  include "inc/database.php";
  $dbh = $result->query("SELECT `Titre`,`Actif` FROM `page` WHERE `Titre`='Nutrigéo' ");
  foreach ($dbh as $row){
    if ($row['Actif'] == "0"){
      header('location:index.php');
      exit;
    }
  }
?>
<!doctype html>
<html lang="fr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link  rel="icon"  href="image/favicon.ico"  type="image/x-icon">

    <!-- google font -->
    
    <!-- js -->
    <!-- <script src="js/main.js"></script> -->
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="css/leaflet.awesome-markers.css"/>
    <link rel="stylesheet" href="css/leaflet.css"/>
    <link rel="stylesheet" href="css/leaflet-sidebar.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    
    <title>Nutrigeo</title>

  </head>

  <body>

    <!-- logo produit  -->

  <header class="header-nutrigeo">

  <?php include "./inc/navbar.php"; ?>

    <img class="logo-head" src="logo_header/nutrigeo.png" alt="Logo">
    <h1 class="txt-header">Le revitaliseur de sols</h1>
    
  </header>

 <main>

  <div class="container">
    <!-- présentation produit -->

    <section>
      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-body">
              <img src="image/nutrigeo_450.png" alt="Produit nutrigeo">
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-body">

            <div class="txt-nutrigeo">

              <p>Améliore la structure du sol <i class="fas fa-check"></i></p>
              <p>Dynamise les échanges minéraux <i class="fas fa-check"></i></p>
              <p>Des résultats visibles en moins de 6 mois ! <i class="fas fa-check"></i></p>

            </div>
   
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- fiche technique -->

    <!-- card full -->

  
    
    <section class="fich-tech container">
    <div class="row text-center">

      <div class="col-sm-4">
        
          <p class="pictos-title"><strong>Cultures industrielles</strong></p>
          <div>
            
            <p class="col-sm"><img src="pictos/colza.PNG" alt="Colza"><br>Colza</p>
            <p class="col-sm"><img src="pictos/pomme-terre.PNG" alt="Pommes de Terre"><br>Pommes de Terre</p>
            <p class="col-sm"><img src="pictos/bettrave.PNG" alt="Carottes Légumes"><br>Betteraves</p>

          </div>
       
      </div>
        
      <div class="col-sm-4">
        
          <p class="pictos-title"><strong>Grandes cultures</strong></p>
          <div>
            
            <p class="col-sm"><img src="pictos/blé.PNG" alt="Blé"><br>Céréales</p>
            <p class="col-sm"><img src="pictos/mais.PNG" alt="Maïs"><br>Maïs</p>

          </div>
        
      </div>
        
      <div class="col-sm-4">
        
          <p class="pictos-title"><strong>Cultures spéciales</strong></p>
          <div>
            
            <p class="col-sm"><img src="pictos/vigne.PNG" alt="vigne"><br>Vigne</p>
            <p class="col-sm"><img src="pictos/fraise.PNG" alt="Arboriculture"><br>Arboriculture</p>
            <p class="col-sm"><img src="pictos/carotte.PNG" alt="Carottes Légumes"><br>Carottes Légumes</p>

          </div>
        
      </div>

    </div>
    </section>

   <!-- boite d'image : témoignage + video  -->
   
   <section>
    <!-- L'id doit correspondre a iframe  + nom du document (document.title) -->
    <div class="iframe" id="iframeNutrigeo">
      
    </div>
   </section>
   
       <!-- Autre section qui continuent -->


    <!-- formulaire + calculette...................................................-->

      <?php include 'inc/geomap.php';?>
            

      <!-- <button type="submit" donwload  -->
        
 </div>
 </main>
 
 <footer id="footer">
        
  <!-- banner cookie -->
  <div class="btncookie" id="boxcookie">
    <p class="p-cookie">Nous utilisons des cookies pour vous garantir la meilleure expérience et améliorer la performance de notre site.</p>
     <p>Pour plus d’informations, consultez nos <a class="rgpd-cookie" href="https://gaiago.eu/legal">registre sur la RGPD</a>. En continuant votre navigation, vous acceptez le dépôt des cookies.</p>
    
    <button id="nocookie" type="button" class="btn btn-outline-light">Non</button>
    <button id="okcookie" type="button" class="btn btn-outline-light">Oui</button>
    
  </div>
 
  <div class="container">
    <div class="row">
 
    <div class="col-sm">

        <p>&copy; Gaiago</p>
    
        <a href="https://twitter.com/gaiago_agri" class="color-2"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z" fill="currentColor"></path></svg></a>
        <a href="https://www.linkedin.com/company/gaiago/" class="color-2"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z" fill="currentColor"></path></svg></a>
        <a href="https://www.facebook.com/gaiago.eu/?ref=bookmarks" class="color-2"><svg viewBox="0 0 448 512"><path d="M448 56.7v398.5c0 13.7-11.1 24.7-24.7 24.7H309.1V306.5h58.2l8.7-67.6h-67v-43.2c0-19.6 5.4-32.9 33.5-32.9h35.8v-60.5c-6.2-.8-27.4-2.7-52.2-2.7-51.6 0-87 31.5-87 89.4v49.9h-58.4v67.6h58.4V480H24.7C11.1 480 0 468.9 0 455.3V56.7C0 43.1 11.1 32 24.7 32h398.5c13.7 0 24.8 11.1 24.8 24.7z" fill="currentColor"></path></svg></a>

    </div>
    <div class="col-sm">

        <p>CONTACT</p>
        <a href="tel:+33299887391"><i class="fas fa-phone-volume"></i> 02 99 88 73 91</a><br>
        <a href="mailto:serviceclient@gaiago.eu"><i class="far fa-envelope"> </i> serviceclient@gaiago.eu</a><br>
        
    </div>
    <div class="col-sm">

        <p>Solutions</p>
        <a href="https://gaiago.eu/solutions">Nos Produits</a>
    
    </div>
    <div class="col-sm">
        <p>Mentions légales</p>
        <a href="https://gaiago.eu/legal">Mentions et RGPD</a>
    </div>     
    
    <!-- <div class="col-sm">
        <a class="btn btn-outline-warning" href="index.html" role="button">FreeN100</a>
        <a class="btn btn-outline-warning" href="admin/index.php" role="button">dashboard</a>
    </div> -->
    
</div>
</div>
</footer>
    
    <!--then Bootstrap JS
    Optional JavaScript :
    jQuery first, then Popper.js 
    then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
    <script src="js/main.js"></script>
    
    <!-- Script carte -->
    <script src="js/leaflet.js"></script>
    <script src="js/leaflet.awesome-markers.min.js"></script>
    <script src="leaflet-gsheets.js"></script>
    
  </body>

</html>
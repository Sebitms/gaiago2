// script pour les cookies (aucun cookie publicitaire juste fonctionnel)

// script banderole cookie
var acces2 = undefined
readcookie();
function readcookie() {
  var name = "cookiecoo=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      acces2 = c.substring(name.length, c.length);
    }
  }
}
if (acces2 == 1) {
  $("#boxcookie").addClass("sm_hidden")
}
$("#okcookie").click(function(){
  $("#boxcookie").addClass("sm_hidden")
  var exdays = 30;
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = "cookiecoo=1; expires="+ expires +"; path=/";
  acces2 = 1
  readcookie(); 
})
$("#nocookie").click(function(){
  $("#boxcookie").addClass("sm_hidden")
})

// Script pour le formulaire de la page Free N100 ( si le formulaire est correctement rempli on met un cookie pour empêcher sa réapparition)

$('#formulaire').submit(function (){
    
event.preventDefault();
$("#formulaire").hide( "fade", { direction: "down" }, "slow" );
$( "#calculette" ).show( "fade", 1000 );
  if (acces2 == 1){
  var exdays = 30;
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = "acceptcoo=1; expires="+ expires +"; path=/";
  }
})

var name = "acceptform=";
var decodedCookie = decodeURIComponent(document.cookie);
var ca = decodedCookie.split(';');
for(var i = 0; i <ca.length; i++) {
  var c = ca[i];
  while (c.charAt(0) == ' ') {
    c = c.substring(1);
  }
  if (c.indexOf(name) == 0) {
    var acces = c.substring(name.length, c.length);
  }
}
if (acces == "1") {
    $("#formulaire").hide( "fade", { direction: "down" }, "slow" );
    $( "#calculette" ).show( "fade", 1000 );
}

// Calculette Azote
function range (){
  var ran = $("#range-azote").val();
  document.getElementById("Tazote").innerHTML = ran + " U/N";
  var surf = $('#surface').val();
  var rend = $('#prix').val();
  var culture = $('#culture2 option:selected').text();
  
  if ( ran != "Votre taux d'azote" && ran != 0 && surface != "" && prix != "" && culture2 != "Veuillez choisir votre culture"){
    $.ajax({
      url : 'data/calc.php',
      type : 'post',
      data : {range: ran, surface : surf, rendement : rend, culture2 : culture},
      datatype: 'JSON',
      success : function(rsp){
        var reponse = JSON.parse(rsp);
        $('#rendement').html(reponse['revenu'] +' €');
        $('#resultat').html(reponse['azote'] + ' U/N');
      },

      error : function(){

      }
   });
  }
}

// Permet de lancer de lancer un appel ajax selon le titre de la page.
// Ici on lis le titre de la page (<title>Nutrigeo</title>) et on execute la fonction lecture en envoyant ce meme titre
if (document.title == "Nutrigeo"){
  lecture("Nutrigeo");
} else if (document.title == "FreeN100"){
  lecture("FreeN100");
}
// Appel ajax permettant de lire la base donnée afin de récupérer l'adresse iframe afin de l'afficher sur la page
function lecture (adresse){
  $.ajax({
    // Ici on choisi la page qu'on vas appellé 
    url : 'data/script.php',
    // Le type d'appel GET ou POST
    type : 'post',
    // Ici les donnée envoyé : dans le cas present on envoie la variable "code : Nutrigéo"
    data : {code : adresse},
    // Le type de donnée que l'appel ajax doit récupérer
    datatype: 'JSON',

    // En cas de succées on execute ceci
    success : function(rsp){
      var iframe = $("#iframe"+adresse)
      iframe.html("<div class='embed-responsive embed-responsive-16by9'>"+rsp)
    },

    // En cas d'erreur on execute ceci
    error : function(){
        $('#resultat').html("L'adresse n'a pas correctement été enregistré")
    }
  });
}
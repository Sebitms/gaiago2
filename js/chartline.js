google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
    var data = google.visualization.arrayToDataTable([
    ["Element", "rendement", { role: "style" } ],
    ["Témoin", 84.5, "orange"],
    ["FreeN100", 91.2, "green"],
    ]);

    var view = new google.visualization.DataView(data);
    view.setColumns([0, 1,
                        { calc: "stringify",
                        sourceColumn: 1,
                        type: "string",
                        role: "annotation" },
                        2]);
    
    var options = {
    title: "Rendement FreeN100 sur BTH",
    width: 600,
    height: 400,
    legend: { position: "none" },
    bar: {groupWidth: "95%"},

    vAxis: {
        baseline:80,
    }
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('curve_chart'));

    chart.draw(data, options);
}
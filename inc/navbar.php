      <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light nav-inc">
        <img class="navbar-brand logo-nav" src="./image/gltgbm6i.png" alt="logo">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link a-drop-nav" href="https://gaiago.eu">Home</a>
            </li>
          
            <li class="nav-item dropdown active a-drop-nav">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Pages produits
              </a>
              <div class='dropdown-menu' aria-labelledby='navbarDropdown'>

              <!-- Change les liens dispo présent dans le header selon sa disponibilité   -->

              <?php
              include "database.php";
              $dbh = $result->query("SELECT * FROM `page` WHERE `Actif` = '1' ");
              foreach($dbh as $row){
                if ($row['Titre'] == "Free N 100" && $row['Actif'] == "1"){
                  echo " <a class='dropdown-item' href='./freen100.php'>Free N100</a>
                        <div class='dropdown-divider'></div>";      
                } elseif ($row['Titre'] == "Nutrigéo" && $row['Actif'] == "1"){            
                  echo "<a class='dropdown-item' href='./nutrigeo.php'>Nutrigéo</a>
                        <div class='dropdown-divider'></div>";
                } elseif ($row['Titre'] == "Free PK" && $row['Actif'] == "1"){
                  echo "<a class='dropdown-item' href='./freepk.php'>Free PK</a>
                  <div class='dropdown-divider'></div>";
                } elseif ($row['Titre'] == "Alasso" && $row['Actif'] == "1"){
                  echo "<a class='dropdown-item' href='./alasso.php'>Alasso</a>
                  <div class='dropdown-divider'></div>";
                } elseif ($row['Titre'] == "Stimulus" && $row['Actif'] == "1"){
                  echo "<a class='dropdown-item' href='./stimulus.php'>Stimulus</a>";
                } elseif ($row['Titre'] == "Vitamin" && $row['Actif'] == "1"){
                  echo "<div class='dropdown-divider'></div>
                        <a class='dropdown-item' href='./vitamin.php'>Vitamin</a>";
                }
              }
              ?>
              </div>
             
            </li>
            
          </ul>
         
        </div>
      </nav>

<?php

  // $servername = 'gaiagoeufend.mysql.db';
  // $dbname = 'gaiagoeufend';
  // $username = 'gaiagoeufend';
  // $password = 'y3BN7Egt4j3UiD2C5c4i';
  
  // //On essaie de se connecter
  // try{
  //     $result = new PDO("mysql:host=$servername;charset=utf8;dbname=$dbname", $username, $password);
  //     //On définit le mode d'erreur de PDO sur Exception
  //     $result->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  //     // echo 'Connexion réussie';
  // }
  
  // /*On capture les exceptions si une exception est lancée et on affiche
  //  *les informations relatives à celle-ci*/
  // catch(PDOException $e){
  //   echo "Erreur : " . $e->getMessage();
  // }

   $servername = 'localhost';
  $dbname = 'gaiago';
  $username = 'root';
  $password = '';
  
  //On essaie de se connecter
  try{
      $result = new PDO("mysql:host=$servername;charset=utf8;dbname=$dbname", $username, $password);
      //On définit le mode d'erreur de PDO sur Exception
      $result->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      // echo 'Connexion réussie';
  }
  
  /*On capture les exceptions si une exception est lancée et on affiche
   *les informations relatives à celle-ci*/
  catch(PDOException $e){
    echo "Erreur : " . $e->getMessage();
  }

?>
  <!--Html nécessaire a l'intégration de la map  -->
  <?php
  
  $dbh = $result->query('SELECT `Titre`,`Actif` FROM `page` WHERE `Titre` = "map" ');
  foreach($dbh as $row){
  ?>
    <section <?php  if ($row['Actif'] == 0){ echo "style='display: none;'";} ?> class="txt_form">
   
  <?php }?>
        <h4 class="txt-annonce-calcul">Vérifier dès maintenant nos points de vente à proximité de chez vous.</h4>
              
        <form class="" id="formmap">

                <div class="choixmap">

                    <input class="form-control small-form" id='codepostal' type="number" placeholder="Entrez votre code postal">
                    <p class="alert-danger" id="resultfail"></p>
                    
                </div>

                <button type="submit" class="btn btn-success"  value="Envoyez"><i class="fas fa-search-location"></i> Envoyez</button>

          </form>

          <div id="map"></div>
          
    </section>
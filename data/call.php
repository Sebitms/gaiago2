<?php
    include "../inc/database.php";
    $dbh = $result->query("SELECT `2020`,`Nom`,`Adresse`,`Adresse2`,`Telephone`,`Code_ville`,`ville_code_postal`,`ville_longitude_deg`,`ville_latitude_deg` 
    FROM `point de ventes` 
    INNER JOIN `villes_france_free` 
    ON `point de ventes`.`Code_ville` = `villes_france_free`.`ville_code_postal` 
    WHERE `2020` = 'x' 
    GROUP BY `point de ventes`.`Code_ville`");
    if (isset($_POST['adresse'])){
        $newadresse = substr($_POST['adresse'],0,2);
        if ((strlen($_POST['adresse']) != 2 &&
            strlen($_POST['adresse']) != 5) || 
            intval($newadresse) > 95){
            echo json_encode([
                "code" => "WRONG"
                ]);
                exit;
        }
        foreach($dbh as $row){
            if ($row['Code_ville'] == $_POST['adresse']){
                echo json_encode([
                    "ville" => $row['Code_ville'],
                    "longitude" => $row['ville_longitude_deg'],
                    "latitude" => $row['ville_latitude_deg']
                    ]);
                exit;
            }elseif(substr($row['Code_ville'],0,2) == $newadresse){
                echo json_encode([
                    "ville" => $row['Code_ville'],
                    "longitude" => $row['ville_longitude_deg'],
                    "latitude" => $row['ville_latitude_deg']
                    ]);
                exit;
            }
        }
        
        $dbh = $result->query("SELECT `ville_departement`,`ville_longitude_deg`,`ville_latitude_deg` FROM `villes_france_free` GROUP BY `ville_departement`");
        foreach($dbh as $row){
            if($row['ville_departement'] == $newadresse){
                echo json_encode([
                    "ville" => $row['ville_departement'],
                    "longitude" => $row['ville_longitude_deg'],
                    "latitude" => $row['ville_latitude_deg'],
                    "code" => "TRUE"
                    ]);
                    exit;
            }
        }   
    }

    
    if (isset($_POST['point'])){
        $data = array();
        foreach($dbh as $row){
            $tel = wordwrap($row['Telephone'],2,".", true);
            array_push($data, array("Nom" => $row['Nom'],"longitude" => $row['ville_longitude_deg'],"latitude" => $row['ville_latitude_deg'],"Adresse"=>$row['Adresse'],"Telephone" =>$tel));
        }
        $json = json_encode($data,JSON_UNESCAPED_UNICODE);
        echo $json;
        exit;
    }
?>
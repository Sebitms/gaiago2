<?php
// Ici on recoit la variable contenant le nom de la page dans le cas présent "code : Nutrigeo"
    if (isset($_POST['code'])){
        // ici id prend la valeur "Nutrigéo"
        $id = $_POST['code'];
        
        // on verifie l'identifiant pour verifier qu'elle ligne de la base de données doit etre lue
        if ($id == "FreeN100"){
            $code = 1;
        } else if ($id == "Nutrigeo") {
            $code = 2;
        }
        // on fait une demande a la base de données pour recuperer l'adresse iframe
        include "../inc/database.php";
        $dbh = $result->prepare('SELECT Adresse FROM iframe WHERE ID = ? ');
        $dbh->execute(array($code));

        // on renvoie l'adresse obtenu en echo a l'appel l'ajax
        foreach($dbh as $row){
            echo $row[0];
        }
        exit;
    }
?>
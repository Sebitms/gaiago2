<?php
   // Tableaux de donnée, le premier chiffre est une valeur en pourcentage 
   // le deuxieme le prix euros / tonne

   include "../inc/database.php";

   $dbh = $result->query(
      "SELECT `Culture`,`Chiffre` 
       FROM `calculatrice` 
       WHERE `Chiffre` != ''  ");

   $data = array();
   foreach ($dbh as $row){
      $data[$row['Culture']] = $row['Chiffre'];  
   }
   
   $culture = $data[$_POST['culture2']];
   $revenu = round((($_POST['surface']*$_POST['rendement'])*0.1)*$culture);
   $azote = round((($_POST['surface']*$_POST['range'])*1.05)*0.3);
   echo json_encode(["revenu" => $revenu,"azote" => $azote]);
?>




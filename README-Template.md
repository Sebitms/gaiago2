# Nom du projet

Pages vitrine pour les produits Gaiago : Free N100, Nutrigéo, Free PK, Alasso, Stimulus, Vitam'in.

## Description

Ce site contient 6 pages vitrine ainsi que une partie Administration pour modifier les differentes fonctionalité du site. Voici une liste des fonctionalités :

- Changez les liens des vidéo pour Nutrigéo et Free N100
- Calculatrice pour le projet Free N100.
- Map pour les points de vente ( disponible sur toutes les pages excepté Free N100 ).
- Possibilité de modifié l'image d'en tete de chaque produit.
- Possibilité de désactivé et remettre en ligne chaque page individuellement.

### Technologie

Ce site est dévelopé en utilisant Html 5, CSS, PHP 7.2.18 et Javascript ( librairie Jquerry ).

## Pages vitrines

Explain how to run the automated tests for this system

### Partie Front End

Explain what these tests test and why

### Partie Back End

#### Video youtube FreeN 100 et Nutrigéo

Les liens sont directement stockée dans la base de donnée a la table 'iframe' puis appelé a partir d'un appel ajax (js/main.js vers data/script.php).

#### Calculette Free N100

Calcul effectué : 

- Premier calcul  :  surface * valeur * (10%) * Rendement Gaiago  = Gain de rendement par hectare avec Freen100, résultat en euros.

- Deuxieme calcul : azote * surface  * 1,05  * (30%) = Economie d'azote résultat en U/N.

La demande à été faite que le calcul soit cachée, donc effectué coté serveur et non client. En suit donc un appel ajax (js/main.js vers data/calc.php).

Le taux d'azote, la surface ainsi que le rendement sont fournit directement par l'utilisateur, le rendement Gaiago lui existe sur la base de donnée

#### Map 

Map réalisé à partir de la librairie Lealflet.
Fichier Nécéssaire : leaflet.js; leaflet-gsheets.js et call.php.

## Partie Administration

Explain how to run the automated tests for this system

### Partie Front End

Explain what these tests test and why

```
Give an example
```

### Partie Back End
Le dashboard se manipule a l'aide d'appel Ajax 

### What's included

Within the download you'll find the following directories and files:

```

|---- README-Template.md
|---- admin
|     |---- assets
|     |     |---- css/
|     |     |---- fonts/
|     |     |---- js/
|     |     |     |---- core/
|     |     |     |---- paper-dashboard.js
|     |     |     |---- paper-dashboard.js.map
|     |     |     |---- paper-dashboard.min.js
|     |     |     L…. plugins/
|     |     |            |---- bootstrap-notify.js
|     |     |            |---- chartjs.min.js
|     |     |            L… perfect-scrollbar.jquery.min.js
|     |     L.. scss/
|     |           |--- paper-dashboard/
|     |           |     |--- cards/
|     |           |     |--- mixins/
|     |           |     L... plugins/
|     |           L... paper-dashboard.scss
|     |---- docs/
|            L... documentation.html
|     |---- html-dashboard/
|           |---- alasso.php
|           |---- datamap.php
|           |---- freen100.php
|           |---- freepk.php
|           |---- nutrigeo.php
|           |---- script.php
|           |---- stimulus.php
|           |---- upload.php
|           L…. vitamin.php
|---- dashboard.php
|---- index.php
|---- login.php
|---- gulpfile.js
|---- package.json
L…..
|---- css/
|     |---- images/
|     |---- leaflet-sidebar.css
|     |---- leaflet.awesome-markers.css
|     |---- leaflet.css
|     L…. main.css
|---- data/
|     |---- calc.php
|     |---- call.php
|     L…. script.php
|---- image/
|---- inc/
|     |---- database.php
|     |---- geomap.php
|     L…. navbar.php
|---- js/
|     |---- chartline.js
|     |---- leaflet.awesome-markers.js
|     |---- leaflet.js
|     L…. main.js
|---- logo_header/
|---- pictos/
|---- gaiago.sql
|---- leaflet-gsheets.js
|---- 404.php
|---- freen100.php
|---- nutrigeo.php
|---- freepk.php
|---- alasso.php
|---- stimulus.php
|---- vitamin.php
|---- index.php
L………...   .gitlab-ci.yml



```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.


## Autheur

* **Kévin Tourat** - *Partie Frontend* - 
* * **Delalande Sébastien** - *Partie Backend* -

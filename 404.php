<!doctype html>
<html lang="fr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link  rel="icon"  href="image/favicon.ico"  type="image/x-icon">

    <!-- google font -->
    
    <!-- js -->
    <!-- <script src="js/main.js"></script> -->
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- style css -->
    

    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    
    <title>Erreur 404</title>

  </head>

  <body class="fixe-size-404">

    <!-- logo produit  -->

    <header class="header-no-found">

      <h3 class="txt-404">Nos services sont actuellement indisponible</h3>

    </header>

  <main class="container body-no-found">
    
  
    
      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-body">            
              <img src="image/error-1349562_1280.png" alt="404"><br>       
            </div>
          </div>
        </div>
        <div class="col-sm-6 ">
          <div class="card">
            <div class="card-body">
              <div class="">
              
             

                  <p>Nôtre site est peut être en maintenance</p>
                  <p>Nous travaillons activement pour la remise en service de nos pages.</p>
                  <p>Nous vous invitons a patientez un moment et d'actualisé votre page.</p>
                  <p>Si le problême persiste, nous sommes à votre disposition ci-dessous ! </p>

                <div class="dropdown-divider"></div>

                  <p>téléphone <a href="tel:+33299887391"><i class="fas fa-phone-volume"></i> 02 99 88 73 91</a></p>
                  <p>mail <a href="mailto:serviceclient@gaiago.eu"><i class="far fa-envelope"> </i> serviceclient@gaiago.eu</a></p>

                <div class="dropdown-divider"></div>

                <div class="col-sm logo-tag">

                  <a href="https://twitter.com/gaiago_agri" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z" fill="currentColor"></path></svg></a>
                  <a href="https://www.linkedin.com/company/gaiago/" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z" fill="currentColor"></path></svg></a>
                  <a href="https://www.facebook.com/gaiago.eu/?ref=bookmarks" ><svg viewBox="0 0 448 512"><path d="M448 56.7v398.5c0 13.7-11.1 24.7-24.7 24.7H309.1V306.5h58.2l8.7-67.6h-67v-43.2c0-19.6 5.4-32.9 33.5-32.9h35.8v-60.5c-6.2-.8-27.4-2.7-52.2-2.7-51.6 0-87 31.5-87 89.4v49.9h-58.4v67.6h58.4V480H24.7C11.1 480 0 468.9 0 455.3V56.7C0 43.1 11.1 32 24.7 32h398.5c13.7 0 24.8 11.1 24.8 24.7z" fill="currentColor"></path></svg></a>
                  <p>&copy; Gaiago</p>
              
                </div>
              
              </div>
            </div>
          </div>
        </div>
      </div>
    

    
    <hr class="hr-bottom-4em">

    

      

  </main>

    
    <div class="pixa-tag">

      <p >Image de <a href="https://pixabay.com/fr/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1349562"> Pixabay</a></p>
      
    </div>
  <footer id="footer-404">

    
    

      
  </footer>
    
    <!--then Bootstrap JS
    Optional JavaScript :
    jQuery first, then Popper.js 
    then Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    
    <!-- Script carte -->
  
    
  </body>

</html>
